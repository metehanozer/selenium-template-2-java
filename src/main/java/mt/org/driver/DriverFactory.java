package mt.org.driver;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static io.github.bonigarcia.wdm.config.DriverManagerType.CHROME;
import static io.github.bonigarcia.wdm.config.DriverManagerType.FIREFOX;
import static java.lang.Boolean.TRUE;

public class DriverFactory implements IDriver {

    public WebDriver createInstance(String browser) {

        WebDriver webdriver;
        switch (browser.toLowerCase()) {

            case "chrome":
                WebDriverManager.getInstance(CHROME).setup();
                //webdriver = new ChromeDriver(getChromeOptions(headless));

                Map<String, Object> prefs = new HashMap<>();
                prefs.put("profile.default_content_setting_values.notifications", 2);
                ChromeOptions options = new ChromeOptions();
                options.setExperimentalOption("prefs", prefs);
                webdriver = new ChromeDriver(options);

                webdriver.manage().window().maximize();
                break;

            case "firefox":
                WebDriverManager.getInstance(FIREFOX).setup();
                //webdriver = new FirefoxDriver(getFirefoxOptions(headless));

                FirefoxProfile profile = new FirefoxProfile();
                profile.setPreference("permissions.default.desktop-notification", 1);
                DesiredCapabilities capabilities = DesiredCapabilities.firefox();
                capabilities.setCapability(FirefoxDriver.PROFILE, profile);
                webdriver = new FirefoxDriver(capabilities);

                webdriver.manage().window().maximize();
                break;

            default:
                throw new IllegalStateException("Unexpected value: " + browser);
        }
        return webdriver;
    }

    private ChromeOptions getChromeOptions(Boolean headless) {
        ChromeOptions options = new ChromeOptions();
        //options.addArguments("--incognito");
        //options.addArguments("disable-infobars");
        //options.addArguments("--disable-geolocation");
        //options.addArguments("--enable-strict-powerful-feature-restrictions");
        //options.setExperimentalOption("excludeSwitches", ["enable-automation"]);
        //options.setExperimentalOption("excludeSwitches", new String[]{"enable-automation"});
        //options.setExperimentalOption("useAutomationExtension", false);
        //options.addArguments("--disable-blink-features");
        //options.addArguments("--disable-blink-features=AutomationControlled");
        //options.setExperimentalOption("excludeSwitches", Collections.singletonList("enable-automation"));
        //options.setExperimentalOption("useAutomationExtension", false);
        //options.addArguments("user-agent=\"Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)\"");
        if (TRUE.equals(headless)) {
            options.setHeadless(true);
        }
        return options;
    }

    private FirefoxOptions getFirefoxOptions(Boolean headless) {
        FirefoxOptions options = new FirefoxOptions();
        options.addArguments("--disable-geolocation");
        if (TRUE.equals(headless)) {
            options.setHeadless(true);
        }
        return options;
    }
}

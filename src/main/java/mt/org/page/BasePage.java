package mt.org.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.List;

public class BasePage {

    private static final int TIMEOUT = 15;
    private static final int POLLING = 100;

    public WebDriver driver;
    private WebDriverWait wait;

    public BasePage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, TIMEOUT, POLLING);
    }

    public String _getCurrentUrl() {
        // DOĞRU GÜNCEL ADRESİ ALMAK İÇİN BİRAZ BEKLENİYOR
        try {Thread.sleep(500);} catch (InterruptedException e) {e.printStackTrace();}
        return driver.getCurrentUrl();
    }

    public void _refreshPage() {
        driver.navigate().refresh();
        try {Thread.sleep(500);} catch (InterruptedException e) {e.printStackTrace();}
    }

    public void _navigateTo(String url) {
        driver.navigate().to(url);
        try {Thread.sleep(500);} catch (InterruptedException e) {e.printStackTrace();}
    }

    //Click Method by using JAVA Generics (You can use both By or Webelement)
    public <T> void _clickTo(T elementAttr) {
        if (elementAttr.getClass().getName().contains("By")) {
            wait.until(ExpectedConditions.elementToBeClickable((By) elementAttr));
            WebElement element = driver.findElement((By) elementAttr);
            element.click();
        } else {
            wait.until(ExpectedConditions.elementToBeClickable((WebElement) elementAttr));
            ((WebElement) elementAttr).click();
        }
        try {Thread.sleep(500);} catch (InterruptedException e) {e.printStackTrace();}
    }

    //Click Method by using JAVA Generics (You can use both By or Webelement)
    public <T> void _hoverTo(T elementAttr) {
        if(elementAttr.getClass().getName().contains("By")) {
            WebElement element = driver.findElement((By) elementAttr);
            Actions actions = new Actions(driver);
            actions.moveToElement(element).perform();
        } else {
            Actions actions = new Actions(driver);
            actions.moveToElement((WebElement) elementAttr).perform();
        }
        // HOVER İŞLEMİNDEN SONRA BİRAZ BEKLENİYOR
        try {Thread.sleep(500);} catch (InterruptedException e) {e.printStackTrace();}
    }

    //Write Text by using JAVA Generics (You can use both By or Webelement)
    public <T> void _sendKeysTo(T elementAttr, String text) {
        if(elementAttr.getClass().getName().contains("By")) {
            wait.until(ExpectedConditions.visibilityOfElementLocated((By) elementAttr));
            WebElement element = driver.findElement((By) elementAttr);
            element.clear();
            element.sendKeys(text);
        } else {
            wait.until(ExpectedConditions.elementToBeClickable((WebElement) elementAttr));
            ((WebElement) elementAttr).clear();
            ((WebElement) elementAttr).sendKeys(text);
        }
        try {Thread.sleep(500);} catch (InterruptedException e) {e.printStackTrace();}
    }

    //Read Text by using JAVA Generics (You can use both By or Webelement)
    public <T> String readText (T elementAttr) {
        if(elementAttr.getClass().getName().contains("By")) {
            return driver.findElement((By) elementAttr).getText();
        } else {
            return ((WebElement) elementAttr).getText();
        }
    }

    //Close popup if exists
    public void handlePopup (By by) throws InterruptedException {
        List<WebElement> popup = driver.findElements(by);
        if(!popup.isEmpty()){
            popup.get(0).click();
            Thread.sleep(200);
        }
    }

    public WebElement _getElementByIndex(By by, int index) {
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(by));
        List<WebElement> elementList = driver.findElements(by);
        return elementList.get(index);
    }

    public List<WebElement> _getElements(By by) {
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(by));
        return driver.findElements(by);
    }

    private WebElement waitForFindVisible(By by) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(by));
        return driver.findElement(by);
    }
}

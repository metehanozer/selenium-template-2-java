package mt.org.page;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import static org.assertj.core.api.Assertions.assertThat;

public class HomePage extends BasePage {

    By myAccountDiv = By.id("myAccount");
    By loginA = By.id("login");
    By searchInput = By.cssSelector(".desktopOldAutosuggestTheme-input");
    By searchButton = By.cssSelector(".SearchBoxOld-buttonContainer");

    public HomePage (WebDriver driver) {
        super(driver);
    }

    @Step("1.  https://www.hepsiburada.com/ sitesine gidecek ve anasayfasının açıldığı onaylayacak.")
    public void openAndVerifyHomePage(String url) {
        _navigateTo(url);
        assertThat(_getCurrentUrl()).isEqualTo(url);
    }

    public LoginPage openLoginPage() {
        _hoverTo(myAccountDiv);
        _clickTo(loginA);
        return new LoginPage(driver);
    }

    @Step("3.  Ekranın üstündeki Search alanına 'samsung' yazıp Ara butonuna tıklayacak.")
    public SearchResultPage searchKeyword(String keyword) {
        _sendKeysTo(searchInput, keyword);
        _clickTo(searchButton);
        return new SearchResultPage(driver);
    }
}

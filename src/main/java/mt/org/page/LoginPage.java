package mt.org.page;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage extends BasePage {

    By txtUserName = By.id("txtUserName");
    By txtPassword = By.id("txtPassword");
    By btnLogin = By.id("btnLogin");

    public LoginPage (WebDriver driver) {
        super(driver);
    }

    @Step("2.  Login ekranını açıp, bir kullanıcı ile login olacak.")
    public HomePage login(String email, String pass) {
        _sendKeysTo(txtUserName, email);
        _sendKeysTo(txtPassword, pass);
        _clickTo(btnLogin);
        return new HomePage(driver);
    }
}

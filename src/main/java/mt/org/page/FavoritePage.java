package mt.org.page;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import java.util.ArrayList;
import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;

public class FavoritePage extends BasePage {

    By productItem = By.cssSelector(".product-item");
    By selectAllButton = By.cssSelector("button#StickActionHeader-Name");
    By removeSelectedButton = By.cssSelector("button#StickActionHeader-RemoveSelected");
    By confirmRemoveButton = By.cssSelector("#DeleteConfirmationModal-ActionButton");

    public FavoritePage(WebDriver driver) {
        super(driver);
    }

    private List<String> getProductItemList(List<WebElement> elements) {
        List<String> productItemList = new ArrayList<>();
        for (WebElement element: elements) {
            String elementId = element.findElement(By.cssSelector("div")).getAttribute("id").replace("ProductBox-", "");
            productItemList.add(elementId);
        }
        return productItemList;
    }

    @Step("8.  Açılan sayfada bir önceki sayfada izlemeye alınmış ürünün bulunduğunu onaylayacak.")
    public void verifyProductIsExist(String selectedProduct) {
        List<WebElement> elements = _getElements(productItem);
        List<String> productItemList = getProductItemList(elements);
        assertThat(productItemList)
                .contains(selectedProduct);
    }

    @Step("9.  Favorilere alınan bu ürünün yanındaki 'Kaldır' butonuna basarak, favorilerimden çıkaracak.")
    public void removeItem() {
        _clickTo(selectAllButton);
//        List<WebElement> elements = getElements(productItem);
//        for (WebElement element: elements) {
//            System.out.println(element.findElement(By.cssSelector("div")).getAttribute("id"));
//            if (element.findElement(By.cssSelector("div")).getAttribute("id").contains(selectedProduct)) {
//                System.out.println("Bulundu");
//                WebElement element1 = element.findElement(By.cssSelector("div > .hNITEF.rve5ysbuac.sc-bZQynM"));
//                hoverTo(element1);
//                element1.findElement(By.cssSelector("div > svg")).click();
//                //WebDriverWait wait = new WebDriverWait(driver, 10);
//                //WebElement element1 = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".gkfviuz1e7.kJHwko.sc-EHOje  .kpESSo.mo6pjnjlx5.sc-gzVnrw > svg")));
//                //element1.click();
//            }
//        }
        _clickTo(removeSelectedButton);
        _clickTo(confirmRemoveButton);
    }

    @Step("10. Sayfada bu ürünün artık favorilere alınmadığını onaylayacak. ")
    public void verifyProductIsNotExist(String selectedProduct) {
        _refreshPage();
        try {
            List<WebElement> elements = _getElements(productItem);
            List<String> productItemList =  getProductItemList(elements);
            assertThat(productItemList)
                    .doesNotContain(selectedProduct);
        } catch (TimeoutException e) {
            System.out.println("Favariler boş.");
        }
    }
}

package mt.org.page;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import static org.assertj.core.api.Assertions.assertThat;

public class SearchResultPage extends BasePage {

    By myAccountDiv = By.id("myAccount");
    By favoriteA = By.cssSelector("a[title='Beğendiklerim']");
    By secondPage = By.cssSelector(".page-2");
    By searchResultList = By.cssSelector(".do-flex.list.product-list.results-container > li");

    public SearchResultPage (WebDriver driver) {
        super(driver);
    }

    @Step("4.  Gelen sayfada samsung için sonuç bulunduğunu onaylayacak.")
    public void verifySearchResultLink(String keyword) {
        assertThat(_getCurrentUrl())
                .contains(String.format("ara?q=%s", keyword));
    }

    @Step("5.  Arama sonuçlarından 2. sayfaya tıklayacak ve açılan sayfada 2. sayfanın şu an gösterimde olduğunu onaylayacak.")
    public void openSecondPageAndVerify(String keyword) {
        _clickTo(secondPage);
        assertThat(_getCurrentUrl())
                .contains(String.format("ara?q=%s&sayfa=2", keyword));
    }

    @Step("6.  Üstten 3. ürünün içindeki 'favorilere ekle' butonuna tıklayacak.")
    public String addToFavorite(int index) {
        WebElement element = _getElementByIndex(searchResultList, index-1);
        element.findElement(By.cssSelector("div#heartWrapper")).click();
        return element.findElement(By.cssSelector("div#heartWrapper")).getAttribute("data-sku");
    }

    @Step("7.  Ekranın en üstündeki 'favorilerim' linkine tıklayacak.")
    public FavoritePage openFavoritePage() {
        _hoverTo(myAccountDiv);
        _clickTo(favoriteA);
        return new FavoritePage(driver);
    }
}

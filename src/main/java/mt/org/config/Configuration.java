package mt.org.config;

import org.aeonbits.owner.Config;
import org.aeonbits.owner.Config.LoadPolicy;
import org.aeonbits.owner.Config.LoadType;

@LoadPolicy(LoadType.MERGE)
@Config.Sources({
    "system:properties",
    "classpath:conf/general.properties"})
public interface Configuration extends Config {

    @Key("url.base")
    String url();

    @Key("email")
    String email();

    @Key("pass")
    String pass();

    @Key("timeout")
    Long timeout();

    @Key("keyword")
    String keyword();
}
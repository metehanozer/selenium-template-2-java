package mt.org;

import mt.org.driver.DriverFactory;
import mt.org.driver.DriverManager;
import mt.org.page.HomePage;
import mt.org.report.AllureManager;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;

@Listeners({TestListener.class})
public abstract class BaseTest {

    public HomePage homePage;

    @BeforeSuite
    public void beforeSuite() {
        AllureManager.setAllureEnvironmentInformation();
    }

    @Parameters({"browser"})
    @BeforeClass(alwaysRun = true)
    public void beforeClass(String browser) {
        System.out.println("beforeClass Browser: " + browser);
        DriverFactory driverFactory = new DriverFactory();
        WebDriver driver = driverFactory.createInstance(browser);
        DriverManager.setDriver(driver);
    }

    @BeforeMethod(alwaysRun=true)
    public void beforeMethod() {
        System.out.println("beforeMethod");
        homePage = new HomePage(DriverManager.getDriver());
    }


    @AfterMethod(alwaysRun = true)
    public void afterMethod() {
        System.out.println("afterMethod");
    }

    @AfterClass(alwaysRun = true)
    public void afterClass() {
        System.out.println("afterClass");
        DriverManager.quit();
    }
}

package mt.org.test;

import mt.org.BaseTest;
import mt.org.config.Configuration;
import mt.org.config.ConfigurationManager;
import mt.org.page.FavoritePage;
import mt.org.page.LoginPage;
import mt.org.page.SearchResultPage;
import org.testng.annotations.Test;

public class HepTest extends BaseTest {

    @Test(description = "checkHep...")
    public void checkHep() throws InterruptedException {
        Configuration conf = ConfigurationManager.getConfiguration();
        String selectedProduct;

        homePage.openAndVerifyHomePage(conf.url());
        LoginPage loginPage = homePage.openLoginPage();
        homePage = loginPage.login(conf.email(), conf.pass());
        SearchResultPage searchResultPage = homePage.searchKeyword(conf.keyword());
        searchResultPage.verifySearchResultLink(conf.keyword());
        searchResultPage.openSecondPageAndVerify(conf.keyword());
        selectedProduct = searchResultPage.addToFavorite(3);
        FavoritePage favoritePage = searchResultPage.openFavoritePage();
        favoritePage.verifyProductIsExist(selectedProduct);
        favoritePage.removeItem();
        favoritePage.verifyProductIsNotExist(selectedProduct);
    }
}
